package com.wiktorkielar.simpleportfoliomanager.database;

import com.wiktorkielar.simpleportfoliomanager.services.Logging;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.wiktorkielar.simpleportfoliomanager.database.Constants.*;

public class Create {

    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS";
    public static final String FOREIGN_KEY = "FOREIGN KEY";
    public static final String REFERENCES = " REFERENCES";

    public static final String CREATE_TABLE_USERS = CREATE_TABLE + " " + TABLE_USERS + " ( " +
            COLUMN_USER_ID + " " + COLUMN_USER_ID_TYPE + ", " +
            COLUMN_USER_FIRST_NAME + " " + COLUMN_USER_FIRST_NAME_TYPE + ", " +
            COLUMN_USER_LAST_NAME + " " + COLUMN_USER_LAST_NAME_TYPE + ", " +
            COLUMN_USER_EMAIL + " " + COLUMN_USER_EMAIL_TYPE + ", " +
            COLUMN_USER_PASS + " " + COLUMN_USER_PASS_TYPE + ", " +
            COLUMN_USER_SALT + " " + COLUMN_USER_SALT_TYPE + ", " +
            COLUMN_USER_REGISTRATION_DATE + " " + COLUMN_USER_REGISTRATION_DATE_TYPE + ", " +
            COLUMN_USER_ACTIVATED + " " + COLUMN_USER_ACTIVATED_TYPE +
            ")";

    public static final String CREATE_TABLE_SYMBOLS = CREATE_TABLE + " " + TABLE_SYMBOLS + " ( " +
            COLUMN_SYMBOL_ID + " " + COLUMN_SYMBOL_ID_TYPE + ", " +
            COLUMN_SYMBOL_NAME + " " + COLUMN_SYMBOL_NAME_TYPE + ", " +
            COLUMN_SYMBOL_TICKER + " " + COLUMN_SYMBOL_TICKER_TYPE + ", " +
            COLUMN_SYMBOL_MARKET_ID + " " + COLUMN_SYMBOL_MARKET_ID_TYPE + ", " +
            FOREIGN_KEY + "(" + COLUMN_SYMBOL_MARKET_ID + ")" +
            REFERENCES + " " + TABLE_EXCHANGES + "(" + COLUMN_EXCHANGE_ID + ")" +
            ")";

    public static final String CREATE_TABLE_TRANSACTIONS = CREATE_TABLE + " " + TABLE_TRANSACTIONS + " ( " +
            COLUMN_TRANSACTION_ID + " " + COLUMN_TRANSACTION_ID_TYPE + ", " +
            COLUMN_TRANSACTION_USER_ID + " " + COLUMN_TRANSACTION_USER_ID_TYPE + ", " +
            COLUMN_TRANSACTION_SYMBOL_ID + " " + COLUMN_TRANSACTION_SYMBOL_ID_TYPE + ", " +
            COLUMN_TRANSACTION_BUY_DATE + " " + COLUMN_TRANSACTION_BUY_DATE_TYPE + ", " +
            COLUMN_TRANSACTION_BUY_PRICE + " " + COLUMN_TRANSACTION_BUY_PRICE_TYPE + ", " +
            COLUMN_TRANSACTION_SELL_DATE + " " + COLUMN_TRANSACTION_SELL_DATE_TYPE + ", " +
            COLUMN_TRANSACTION_SELL_PRICE + " " + COLUMN_TRANSACTION_SELL_PRICE_TYPE + ", " +
            COLUMN_TRANSACTION_VOLUME + " " + COLUMN_TRANSACTION_VOLUME_TYPE + ", " +
            FOREIGN_KEY + "(" + COLUMN_TRANSACTION_USER_ID + ")" +
            REFERENCES + " " + TABLE_USERS + "(" + COLUMN_USER_ID + "), " +
            FOREIGN_KEY + "(" + COLUMN_TRANSACTION_SYMBOL_ID + ")" +
            REFERENCES + " " + TABLE_SYMBOLS + "(" + COLUMN_SYMBOL_ID + ")" +
            ")";

    public static final String CREATE_TABLE_EXCHANGES = CREATE_TABLE + " " + TABLE_EXCHANGES + " ( " +
            COLUMN_EXCHANGE_ID + " " + COLUMN_EXCHANGE_ID_TYPE + ", " +
            COLUMN_EXCHANGE_SHORT_NAME + " " + COLUMN_EXCHANGE_SHORT_NAME_TYPE + ", " +
            COLUMN_EXCHANGE_LONG_NAME + " " + COLUMN_EXCHANGE_LONG_NAME_TYPE +
            ")";

    public static final String CREATE_TABLE_TOKENS = CREATE_TABLE + " " + TABLE_TOKENS + " ( " +
            COLUMN_TOKEN_ID + " " + COLUMN_TOKEN_ID_TYPE + ", " +
            COLUMN_TOKEN_USER_ID + " " + COLUMN_TOKEN_USER_ID_TYPE + ", " +
            COLUMN_TOKEN_TOKEN + " " + COLUMN_TOKEN_TOKEN_TYPE + ", " +
            COLUMN_TOKEN_CREATION_DATE + " " + COLUMN_TOKEN_CREATION_DATE_TYPE +
            ")";

    private static PreparedStatement createTableUsersPreparedStatement;
    private static  PreparedStatement createTableTransactionsPreparedStatement;
    private static  PreparedStatement createTableSymbolsPreparedStatement;
    private static  PreparedStatement createTableExchangesPreparedStatement;
    private static  PreparedStatement createTableTokensPreparedStatement;

    public static final String CLASS_NAME = Logging.getClassName();

    private Create(){
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand){
        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.execute();
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object");
        }
    }

    private static void createTable(PreparedStatement preparedStatement, String sqlCommand) {
        Constants.openConnection();
        execute(preparedStatement, sqlCommand);
        Constants.closeConnection(preparedStatement);
    }

    public static void createTables() {
        createTable(createTableExchangesPreparedStatement, CREATE_TABLE_EXCHANGES);
        createTable(createTableSymbolsPreparedStatement, CREATE_TABLE_SYMBOLS);
        createTable(createTableUsersPreparedStatement, CREATE_TABLE_USERS);
        createTable(createTableTransactionsPreparedStatement, CREATE_TABLE_TRANSACTIONS);
        createTable(createTableTokensPreparedStatement, CREATE_TABLE_TOKENS);
    }
}
