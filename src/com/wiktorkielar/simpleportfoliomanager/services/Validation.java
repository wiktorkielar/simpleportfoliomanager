package com.wiktorkielar.simpleportfoliomanager.services;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    private Validation() {
    }

    //TESTED, OK
    public static boolean validateRegistrationString(String fieldType, String inputString) {
        if (fieldType.contains("NAME")) {
            return Validation.isInRange(inputString, 2, 100);
        }
        if (fieldType.contains("EMAIL")) {
            return (Validation.isInRange(inputString, 5, 100) && Validation.isEmail(inputString));
        }
        if (fieldType.contains("PASSWORD")) {
            return (Validation.isInRange(inputString, 10, 100) && Validation.isPassword(inputString));
        }
        return false;
    }

    //TESTED, OK
    private static boolean isInRange(String input, int minLength, int maxLength) {
        return input.length() >= minLength && input.length() <= maxLength;
    }

    //TESTED, OK
    public static boolean isEmail(String input) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.[a-zA-Z]{2,}";
        return patternMatch(input, emailPattern);
    }

    //TESTED, OK
    private static boolean isPassword(String input) {
        String lowerCasePattern = "^.+[a-z].+$";
        String upperCasePattern = "^.+[A-Z].+$";
        String numericalPattern = "^.+[0-9].+$";
        String specialCharactersPattern = "^.+[^A-Za-z0-9].+$";

        return (patternMatch(input, lowerCasePattern) && patternMatch(input, upperCasePattern) &&
                patternMatch(input, numericalPattern) && patternMatch(input, specialCharactersPattern));
    }

    //TESTED, OK
    private static boolean patternMatch(String input, String inputPattern) {
        Pattern pattern = Pattern.compile(inputPattern);
        Matcher matcher = pattern.matcher(input);
        return matcher.matches();
    }

    public static int getIntValue(String input){
        if(isParsableToInteger(input)){
            return Integer.parseInt(input);
        } else {
            return -1;
        }
    }

    public static boolean isParsableToInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    //TESTED, OK
    public static void splitIntoRowsAndDisplay(String input, int maxLength) {
        String[] inputElements = input.split(" ");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < inputElements.length - 1; i++) {
            stringBuilder.append(inputElements[i]);
            stringBuilder.append(" ");
            if (stringBuilder.length() + inputElements[i + 1].length() >= maxLength) {
                System.out.println(stringBuilder.toString());
                stringBuilder.setLength(0);
            }
        }
        stringBuilder.append(inputElements[inputElements.length-1]);
        System.out.println(stringBuilder.toString());
    }


}
