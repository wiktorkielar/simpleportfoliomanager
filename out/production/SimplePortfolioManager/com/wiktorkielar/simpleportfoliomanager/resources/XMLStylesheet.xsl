<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <body>
                <h1>Log file details:</h1>
                <table border="1" cellspacing="0" cellpadding="5" align="left">
                    <tr>
                        <th>Date</th>
                        <th>Millis</th>
                        <th>Sequence</th>
                        <th>Logger</th>
                        <th>Level</th>
                        <th>Class</th>
                        <th>Method</th>
                        <th>Thread</th>
                        <th>Message</th>
                        <th>Exception</th>
                    </tr>
                    <xsl:for-each select="log/record">
                        <tr>
                            <td>
                                <xsl:value-of select="date"/>
                            </td>
                            <td>
                                <xsl:value-of select="millis"/>
                            </td>
                            <td>
                                <xsl:value-of select="sequence"/>
                            </td>
                            <td>
                                <xsl:value-of select="logger"/>
                            </td>
                            <td>
                                <xsl:value-of select="level"/>
                            </td>
                            <td>
                                <xsl:value-of select="class"/>
                            </td>
                            <td>
                                <xsl:value-of select="method"/>
                            </td>
                            <td>
                                <xsl:value-of select="thread"/>
                            </td>
                            <td>
                                <xsl:value-of select="message"/>
                            </td>
                            <td>
                                <xsl:value-of select="exception"/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>