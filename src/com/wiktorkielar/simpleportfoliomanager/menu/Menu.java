package com.wiktorkielar.simpleportfoliomanager.menu;

import com.wiktorkielar.simpleportfoliomanager.services.Validation;

import java.util.*;

public class Menu {

    public static final int TABLE_WIDTH = 45;

    private String header;
    private List<String> positions;

    public Menu(String header) {
        this.header = header;
        this.positions = new ArrayList<>();
    }

    public void addPosition(String position) {
        this.positions.add(position);

    }

    public void displayMenu() {

        printBorderRow();
        printHeaderRow();
        printBorderRow();

        if (!positions.isEmpty()) {
            appendOptionNumbers();
            printDataRows();
            printBorderRow();
        }
    }

    public int readUserInput() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nPlease select option:");
            String input = scanner.nextLine();
            if (Validation.isParsableToInteger(input)) {
                int option = Validation.getIntValue(input);
                for (int i = 0; i < positions.size(); i++) {
                    if (Character.getNumericValue(positions.get(i).charAt(1)) == option) {
                        return option;
                    }
                }
            }

        }
    }

    private void printBorderRow() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("+");
        for (int i = 0; i < TABLE_WIDTH - 2; i++) {
            stringBuilder.append("-");
        }
        stringBuilder.append("+");
        System.out.println(stringBuilder.toString());

    }

    //TESTED, OK
    private void printHeaderRow() {
        StringBuilder stringBuilder = new StringBuilder();

        int padding = TABLE_WIDTH - header.length() - 2;
        int change = padding % 2;
        stringBuilder.append("|");

        for (int i = 0; i < padding / 2; i++) {
            stringBuilder.append(" ");
        }

        stringBuilder.append(header);

        for (int i = 0; i < (padding / 2) + change; i++) {
            stringBuilder.append(" ");
        }
        stringBuilder.append("|");
        System.out.println(stringBuilder.toString());
    }

    //TESTED, OK
    private void appendOptionNumbers() {
        for (int i = 0; i < positions.size(); i++) {
            positions.set(i, " " + (i + 1) + ". " + positions.get(i));
        }
    }

    //TESTED, OK
    private void printDataRows() {

        StringBuilder stringBuilder = new StringBuilder();
        for (String string : positions) {
            stringBuilder.append("|");
            stringBuilder.append(string);

            for (int i = 0; i < TABLE_WIDTH - string.length() - 2; i++) {
                stringBuilder.append(" ");
            }

            stringBuilder.append("|");
            System.out.println(stringBuilder.toString());
            stringBuilder.setLength(0);
        }

    }
}
