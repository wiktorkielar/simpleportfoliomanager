package com.wiktorkielar.simpleportfoliomanager.database;

import com.wiktorkielar.simpleportfoliomanager.services.Logging;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.wiktorkielar.simpleportfoliomanager.database.Constants.*;

public class Update {



    public static final String UPDATE_USERS_ACTIVATED = "UPDATE " + TABLE_USERS + " SET " +
            COLUMN_USER_ACTIVATED + " = 1 " +
            "WHERE " + COLUMN_USER_EMAIL + " = ?";

    public static final String UPDATE_USERS_SALT_AND_PASSWORD = "UPDATE " + TABLE_USERS + " SET " +
            COLUMN_USER_SALT + " = ?, " + COLUMN_USER_PASS + " = ? " +
            "WHERE " + COLUMN_USER_EMAIL + " = ?";

    public static final String CLASS_NAME = Logging.getClassName();
    
    private static PreparedStatement updateUsersActivatedPreparedStatement;
    private static PreparedStatement updateUsersSaltAndPasswordPreparedStatement;

    private Update(){
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand, String email){

        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.setString(1, email);
            preparedStatement.execute();
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object");
        }
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand,
                                byte[] salt, byte[] pass, String email){
        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.setBytes(1, salt);
            preparedStatement.setBytes(2, pass);
            preparedStatement.setString(3, email);
            preparedStatement.execute();

        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object");
        }

    }

    public static void updateUsersActivated(String email){
        Constants.openConnection();
        execute(updateUsersActivatedPreparedStatement, UPDATE_USERS_ACTIVATED, email);
        Constants.closeConnection(updateUsersActivatedPreparedStatement);
    }

    public static void updateUsersSaltAndPassword(byte[] salt, byte[] pass, String email){
        Constants.openConnection();
        execute(updateUsersSaltAndPasswordPreparedStatement,
                UPDATE_USERS_SALT_AND_PASSWORD, salt, pass, email);
        Constants.closeConnection(updateUsersSaltAndPasswordPreparedStatement);
    }


}
