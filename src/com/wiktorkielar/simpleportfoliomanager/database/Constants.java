package com.wiktorkielar.simpleportfoliomanager.database;

import com.wiktorkielar.simpleportfoliomanager.services.Logging;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class Constants {

    public static final String DB_NAME = "SimplePortfolioManager.db";
    public static final String CONNECTION_STRING =
            "jdbc:sqlite:src/com/wiktorkielar/simpleportfoliomanager/resources/" + DB_NAME;

    public static final String TABLE_USERS = "users";
    public static final String TABLE_SYMBOLS = "symbols";
    public static final String TABLE_TRANSACTIONS = "transactions";
    public static final String TABLE_EXCHANGES = "exchanges";
    public static final String TABLE_TOKENS = "tokens";

    public static final String COLUMN_TYPE_INTEGER_PK_AUTOINCREMENT = "INTEGER PRIMARY KEY AUTOINCREMENT";
    public static final String COLUMN_TYPE_TEXT_NOT_NULL = "TEXT NOT NULL";
    public static final String COLUMN_TYPE_BLOB_NOT_NULL = "BLOB NOT NULL";
    public static final String COLUMN_TYPE_INTEGER_NOT_NULL = "INTEGER NOT NULL";
    public static final String COLUMN_TYPE_REAL_NOT_NULL = "REAL NOT NULL";

    public static final String COLUMN_USER_ID = "id";
    public static final String COLUMN_USER_ID_TYPE = COLUMN_TYPE_INTEGER_PK_AUTOINCREMENT;
    public static final String COLUMN_USER_FIRST_NAME = "first_name";
    public static final String COLUMN_USER_FIRST_NAME_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_USER_LAST_NAME = "last_name";
    public static final String COLUMN_USER_LAST_NAME_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_USER_EMAIL = "email";
    public static final String COLUMN_USER_EMAIL_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_USER_PASS = "pass";
    public static final String COLUMN_USER_PASS_TYPE = COLUMN_TYPE_BLOB_NOT_NULL;
    public static final String COLUMN_USER_SALT = "salt";
    public static final String COLUMN_USER_SALT_TYPE = COLUMN_TYPE_BLOB_NOT_NULL;
    public static final String COLUMN_USER_REGISTRATION_DATE = "registration_date";
    public static final String COLUMN_USER_REGISTRATION_DATE_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_USER_ACTIVATED = "activated";
    public static final String COLUMN_USER_ACTIVATED_TYPE = COLUMN_TYPE_INTEGER_NOT_NULL;

    public static final String COLUMN_SYMBOL_ID = "id";
    public static final String COLUMN_SYMBOL_ID_TYPE = COLUMN_TYPE_INTEGER_PK_AUTOINCREMENT;
    public static final String COLUMN_SYMBOL_NAME = "name";
    public static final String COLUMN_SYMBOL_NAME_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_SYMBOL_TICKER = "ticker";
    public static final String COLUMN_SYMBOL_TICKER_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_SYMBOL_MARKET_ID = "market_id";
    public static final String COLUMN_SYMBOL_MARKET_ID_TYPE = COLUMN_TYPE_INTEGER_NOT_NULL;

    public static final String COLUMN_TRANSACTION_ID = "id";
    public static final String COLUMN_TRANSACTION_ID_TYPE = COLUMN_TYPE_INTEGER_PK_AUTOINCREMENT;
    public static final String COLUMN_TRANSACTION_USER_ID = "user_id";
    public static final String COLUMN_TRANSACTION_USER_ID_TYPE = COLUMN_TYPE_INTEGER_NOT_NULL;
    public static final String COLUMN_TRANSACTION_SYMBOL_ID = "symbol_id";
    public static final String COLUMN_TRANSACTION_SYMBOL_ID_TYPE = COLUMN_TYPE_INTEGER_NOT_NULL;
    public static final String COLUMN_TRANSACTION_BUY_DATE = "buy_date";
    public static final String COLUMN_TRANSACTION_BUY_DATE_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_TRANSACTION_BUY_PRICE = "buy_price";
    public static final String COLUMN_TRANSACTION_BUY_PRICE_TYPE = COLUMN_TYPE_REAL_NOT_NULL;
    public static final String COLUMN_TRANSACTION_SELL_DATE = "sell_date";
    public static final String COLUMN_TRANSACTION_SELL_DATE_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_TRANSACTION_SELL_PRICE = "sell_price";
    public static final String COLUMN_TRANSACTION_SELL_PRICE_TYPE = COLUMN_TYPE_REAL_NOT_NULL;
    public static final String COLUMN_TRANSACTION_VOLUME = "volume";
    public static final String COLUMN_TRANSACTION_VOLUME_TYPE = COLUMN_TYPE_REAL_NOT_NULL;

    public static final String COLUMN_EXCHANGE_ID = "id";
    public static final String COLUMN_EXCHANGE_ID_TYPE = COLUMN_TYPE_INTEGER_PK_AUTOINCREMENT;
    public static final String COLUMN_EXCHANGE_SHORT_NAME = "short_name";
    public static final String COLUMN_EXCHANGE_SHORT_NAME_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_EXCHANGE_LONG_NAME = "long_name";
    public static final String COLUMN_EXCHANGE_LONG_NAME_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;

    public static final String COLUMN_TOKEN_ID = "id";
    public static final String COLUMN_TOKEN_ID_TYPE = COLUMN_TYPE_INTEGER_PK_AUTOINCREMENT;
    public static final String COLUMN_TOKEN_USER_ID = "user_id";
    public static final String COLUMN_TOKEN_USER_ID_TYPE = COLUMN_TYPE_INTEGER_NOT_NULL;
    public static final String COLUMN_TOKEN_TOKEN = "token";
    public static final String COLUMN_TOKEN_TOKEN_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;
    public static final String COLUMN_TOKEN_CREATION_DATE = "creation_date";
    public static final String COLUMN_TOKEN_CREATION_DATE_TYPE = COLUMN_TYPE_TEXT_NOT_NULL;

    public static final String COLUMN_ALL = "*";
    public static final String COLUMN_COUNT_ALL_AS_COUNT = "COUNT(*) AS COUNT";

    public static final String STATEMENT_SELECT = "SELECT";
    public static final String STATEMENT_INSERT_INTO = "INSERT INTO";
    public static final String STATEMENT_DELETE = "DELETE";

    public static final String CLAUSE_FROM = "FROM";
    public static final String CLAUSE_WHERE = "WHERE";

    public static final String OPERATOR_AND = "AND";

    public static final String CLASS_NAME = Logging.getClassName();

    private static Connection connection;

    private Constants() {
    }

    public static Connection getConnection() {
        return connection;
    }

    public static void openConnection() {
        try {
            connection = DriverManager.getConnection(CONNECTION_STRING);
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error attempting to establish a connection to the given database URL.");
        }
    }

    public static void closeConnection(PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error closing the connection.");
        }
    }

    public static String getTime(int minutes) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(minutes);
        return localDateTime.format(dateTimeFormatter);
    }
}
