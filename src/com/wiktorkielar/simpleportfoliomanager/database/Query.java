package com.wiktorkielar.simpleportfoliomanager.database;

import com.wiktorkielar.simpleportfoliomanager.objects.Exchange;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.wiktorkielar.simpleportfoliomanager.database.Constants.*;

public class Query {

    public static final String COUNT_COLUMN_ALL = "COUNT(*) AS COUNT";

    public static final String QUERY_USERS_FOR_SALT = STATEMENT_SELECT + " " + COLUMN_USER_SALT + " " + CLAUSE_FROM +
            " " +
            TABLE_USERS +
            " " + CLAUSE_WHERE + " " + COLUMN_USER_EMAIL + " = ?";

    public static final String QUERY_USERS_FOR_PASS_MATCH = STATEMENT_SELECT + " " + COUNT_COLUMN_ALL + " " +
            CLAUSE_FROM + " " + TABLE_USERS +
            " " + CLAUSE_WHERE + " " + COLUMN_USER_EMAIL + " = ? " + OPERATOR_AND + " " + COLUMN_USER_PASS + " = ? "
            + OPERATOR_AND + " " + COLUMN_USER_SALT +
            " = ?";

    public static final String QUERY_USERS_FOR_EMAIL = STATEMENT_SELECT + " " + COUNT_COLUMN_ALL + " " + CLAUSE_FROM
            + " " + TABLE_USERS +
            " " + CLAUSE_WHERE + " " + COLUMN_USER_EMAIL + " = ?";

    public static final String QUERY_USERS_FOR_NAME = STATEMENT_SELECT + " " + COLUMN_USER_FIRST_NAME + " " +
            CLAUSE_FROM + " " +
            TABLE_USERS +
            " " + CLAUSE_WHERE + " " + COLUMN_USER_EMAIL + " = ?";

    public static final String QUERY_EXCHANGES_FOR_SHORT_NAME_AND_LONG_NAME = STATEMENT_SELECT + " " +
            COLUMN_EXCHANGE_SHORT_NAME +
            ", " + COLUMN_EXCHANGE_LONG_NAME + " " + CLAUSE_FROM + " " + TABLE_EXCHANGES;

    public static final String QUERY_EXCHANGES_FOR_ID = STATEMENT_SELECT + " " + COLUMN_EXCHANGE_ID + " " + CLAUSE_FROM
            + " " + TABLE_EXCHANGES + " " + CLAUSE_WHERE + " " + COLUMN_EXCHANGE_SHORT_NAME + " = ?";

    public static final String QUERY_EXCHAGNES_FOR_COUNT = STATEMENT_SELECT + " " + COLUMN_COUNT_ALL_AS_COUNT +
            " " + CLAUSE_FROM + " " + TABLE_EXCHANGES;

    public static final String QUERY_SYMBOLS_FOR_COUNT = STATEMENT_SELECT + " " + COLUMN_COUNT_ALL_AS_COUNT +
            " " + CLAUSE_FROM + " " + TABLE_SYMBOLS;


    private static PreparedStatement queryUsersForSaltPreparedStatement;
    private static PreparedStatement queryUsersForPassMatchPreparedStatement;
    private static PreparedStatement queryUsersForEmailPreparedStatement;
    private static PreparedStatement queryUsersForNamePreparedStatement;
    private static PreparedStatement queryExchangesForShortNameAndLongNamePreparedStatement;
    private static PreparedStatement queryExchangesForIdPreparedStatement;
    private static PreparedStatement queryExchangesForCountPreparedStatement;
    private static PreparedStatement querySymbolsForCountPreparedStatement;

    private Query() {
    }

    private static <T> T executeQuery(String identifier, PreparedStatement queryPreparedStatement, String sqlCommand,
                                      String email, byte[] pass, byte[] salt, String shortName) {
        T returnType = null;

        try {

            queryPreparedStatement = Constants.getConnection().prepareStatement(sqlCommand);

            if (email != null) {
                queryPreparedStatement.setString(1, email);
            }

            if (pass != null && salt != null) {
                queryPreparedStatement.setBytes(2, pass);
                queryPreparedStatement.setBytes(3, salt);
            }

            if (shortName != null) {
                queryPreparedStatement.setString(1, shortName);
            }

            ResultSet resultSet = queryPreparedStatement.executeQuery();


            if (identifier.equals("salt")) {
                while (resultSet.next()) {
                    returnType = (T) resultSet.getBytes("salt");
                }
            }
            if (identifier.equals("pass") || identifier.equals("email")) {
                while (resultSet.next()) {
                    returnType = (T) Integer.valueOf(resultSet.getInt("count"));
                }
            }
            if (identifier.equals("first_name")) {
                while (resultSet.next()) {
                    returnType = (T) resultSet.getString("first_name");
                }
            }
            if (identifier.equals("short_name_and_long_name")) {
                List<Exchange> exchangeList = new ArrayList<>();
                while (resultSet.next()) {
                    Exchange exchange = new Exchange();
                    exchange.setShortName(resultSet.getString("short_name"));
                    exchange.setLongName(resultSet.getString("long_name"));
                    exchangeList.add(exchange);
                }
                returnType = (T) exchangeList;
            }
            if (identifier.equals("id")) {
                while (resultSet.next()) {
                    returnType = (T) Integer.valueOf(resultSet.getInt("id"));
                }
            }
            if (identifier.equals("count_exchanges")) {
                while ((resultSet.next())) {
                    returnType = (T) Integer.valueOf(resultSet.getInt("count"));
                }
            }
            if (identifier.equals("count_symbols")) {
                while ((resultSet.next())) {
                    returnType = (T) Integer.valueOf(resultSet.getInt("count"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return returnType;
    }

    private static byte[] executeQueryUsersForSalt(String email) {
        return executeQuery("salt", queryUsersForSaltPreparedStatement,
                QUERY_USERS_FOR_SALT, email, null, null, null);
    }

    private static Integer executeQueryUsersForPassMatch(String email, byte[] pass, byte[] salt) {
        return executeQuery("pass", queryUsersForPassMatchPreparedStatement,
                QUERY_USERS_FOR_PASS_MATCH, email, pass, salt, null);
    }

    private static Integer executeQueryUsersForEmail(String email) {
        return executeQuery("email", queryUsersForEmailPreparedStatement,
                QUERY_USERS_FOR_EMAIL, email, null, null, null);
    }

    private static String executeQueryUsersForName(String email) {
        return executeQuery("first_name", queryUsersForNamePreparedStatement,
                QUERY_USERS_FOR_NAME, email, null, null, null);
    }

    private static List<Exchange> executeQueryExchangesForShortNameAndLongName() {
        return executeQuery("short_name_and_long_name", queryExchangesForShortNameAndLongNamePreparedStatement,
                QUERY_EXCHANGES_FOR_SHORT_NAME_AND_LONG_NAME, null, null, null, null);
    }

    private static Integer executeQueryExchangesForId(String shortName) {
        return executeQuery("id", queryExchangesForIdPreparedStatement, QUERY_EXCHANGES_FOR_ID, null, null,
                null, shortName);
    }

    private static Integer executeQueryExchangesForCount() {
        return executeQuery("count_exchanges", queryExchangesForCountPreparedStatement, QUERY_EXCHAGNES_FOR_COUNT,
                null, null, null, null);
    }

    private static Integer executeQuerySymbolsForCount() {
        return executeQuery("count_symbols", querySymbolsForCountPreparedStatement, QUERY_SYMBOLS_FOR_COUNT, null, null,
        null, null);
    }


    private static <T> T query(String identifier, PreparedStatement queryPreparedStatement, String email, byte[]
            pass, byte[] salt, String shortName) {

        T returnType = null;

        Constants.openConnection();
        if (identifier.equals("salt")) {
            returnType = (T) executeQueryUsersForSalt(email);
        }
        if (identifier.equals("pass")) {
            returnType = (T) executeQueryUsersForPassMatch(email, pass, salt);
        }
        if (identifier.equals("email")) {
            returnType = (T) executeQueryUsersForEmail(email);
        }
        if (identifier.equals("first_name")) {
            returnType = (T) executeQueryUsersForName(email);
        }
        if (identifier.equals("short_name_and_long_name")) {
            returnType = (T) executeQueryExchangesForShortNameAndLongName();
        }
        if (identifier.equals("id")) {
            returnType = (T) executeQueryExchangesForId(shortName);
        }
        if (identifier.equals("count_exchanges")){
            returnType = (T) executeQueryExchangesForCount();
        }
        if (identifier.equals("count_symbols")){
            returnType = (T) executeQuerySymbolsForCount();
        }
        Constants.closeConnection(queryPreparedStatement);
        return returnType;

    }

    public static byte[] queryUsersForSalt(String email) {
        return query("salt", queryUsersForSaltPreparedStatement, email, null, null,
                null);
    }

    public static Integer queryUsersForPassMatch(String email, byte[] pass, byte[] salt) {
        return query("pass", queryUsersForPassMatchPreparedStatement, email, pass, salt,
                null);
    }

    public static Integer queryUsersForEmail(String email) {
        return query("email", queryUsersForEmailPreparedStatement, email, null, null,
                null);
    }

    public static String queryUsersForName(String email) {
        return query("first_name", queryUsersForEmailPreparedStatement, email, null, null,
                null);
    }

    public static List<Exchange> queryExchangesForShortNameAndLongName() {
        return query("short_name_and_long_name", queryExchangesForShortNameAndLongNamePreparedStatement,
                null, null, null, null);
    }

    public static Integer queryExchangesForId(String shortName) {
        return query("id", queryExchangesForIdPreparedStatement, null, null, null,
                shortName);
    }

    public static Integer queryExchangesForCount(){
        return query("count_exchanges", queryExchangesForCountPreparedStatement, null, null, null, null);
    }

    public static Integer querySymbolsForCount(){
        return query("count_symbols", querySymbolsForCountPreparedStatement, null, null, null, null);
    }


}
