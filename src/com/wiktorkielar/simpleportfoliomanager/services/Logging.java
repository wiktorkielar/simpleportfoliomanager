package com.wiktorkielar.simpleportfoliomanager.services;

import java.io.IOException;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Logging {


    public static final String LOG_PATH = "src/com/wiktorkielar/simpleportfoliomanager/logs/xml/";

    private static Logger logger;
    private static Level level = Level.ALL;
    private static FileHandler fileHandler;

    public static final boolean START_FILE_LOGGING = true;
    public static final boolean START_CONSOLE_LOGGING = true;

    private Logging() {
    }

    public static String getClassName() {
        String[] stackTrace = (new Exception().getStackTrace()[1].getClassName()).split("\\.");
        return stackTrace[stackTrace.length - 1];
    }

    public static void log(String className, Exception e, String exceptionMessage){
        start(className);
        logError(e, exceptionMessage);
        stop(className);
    }

    private static void start(String className) {

        logger = Logger.getLogger(className);
        logger.setLevel(level);

        if (START_FILE_LOGGING) {
            fileHandler = null;
            try {
                fileHandler = new FileHandler(LOG_PATH + className + ".xml");
                fileHandler.setLevel(level);
                logger.addHandler(fileHandler);
            } catch (IOException e) {
                System.out.println("Error creating FileHandler instance in: " + className + ".class. Message: " + e
                        .getMessage());
            }
        }
        if (START_CONSOLE_LOGGING) {
            ConsoleHandler consoleHandler = new ConsoleHandler();
            consoleHandler.setLevel(level);
            logger.addHandler(consoleHandler);
        }
    }

    private static void logError(Exception e, String exceptionMessage) {
        logger.log(level, exceptionMessage, e);
    }

    private static void stop(String className) {
        if (fileHandler != null) {
            fileHandler.close();
        }
        if(START_FILE_LOGGING){
            Transformation.start(className);
        }
    }

}
