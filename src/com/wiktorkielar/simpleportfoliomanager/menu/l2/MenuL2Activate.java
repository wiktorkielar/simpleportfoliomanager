package com.wiktorkielar.simpleportfoliomanager.menu.l2;

import com.wiktorkielar.simpleportfoliomanager.database.Query;
import com.wiktorkielar.simpleportfoliomanager.database.Update;
import com.wiktorkielar.simpleportfoliomanager.menu.Menu;
import com.wiktorkielar.simpleportfoliomanager.menu.l1.MenuL1;
import com.wiktorkielar.simpleportfoliomanager.services.Encryption;
import com.wiktorkielar.simpleportfoliomanager.services.Validation;

import java.util.Scanner;

public class MenuL2Activate {

    private static Scanner scanner = new Scanner(System.in);
    private static String email;
    private static String activationCode;


    public static void start() {
        Menu menuL2Activate = new Menu("Activate New Account");
        menuL2Activate.displayMenu();
        activate();
    }

    public static void activate() {
        verifyEmailAddress();
        checkEmailInDatabase();
        checkActivationCodeMatch();
        updateDatabase();
    }

    private static void verifyEmailAddress() {
        while (email == null || !Validation.isEmail(email)) {
            System.out.println("\nEMAIL ADDRESS:");
            email = scanner.nextLine();
        }
    }

    private static void checkEmailInDatabase() {
        if (Query.queryUsersForEmail(email) != 1) {
            System.out.println("There's no such email in the database!");
            email = null;
            verifyEmailAddress();
        }
    }

    private static void checkActivationCodeMatch() {
        System.out.println("\nACTIVATION CODE:");
        activationCode = scanner.nextLine();
        if (!activationCode.equals(Encryption.generateActivationCode(email))) {
            System.out.println("\nThe activation code is wrong. Please try again.\n");
            email = null;
            start();
        }
    }

    private static void updateDatabase() {
        Update.updateUsersActivated(email);
        Validation.splitIntoRowsAndDisplay("\nCongratulations! Your account has been activated! " +
                "To start using SPM please Log in ([Login] option in main menu). \n", 45);
        email = null;
        MenuL1.start();
    }
}
