package com.wiktorkielar.simpleportfoliomanager.menu.l2;

import com.wiktorkielar.simpleportfoliomanager.database.Insert;
import com.wiktorkielar.simpleportfoliomanager.database.Query;
import com.wiktorkielar.simpleportfoliomanager.menu.Menu;
import com.wiktorkielar.simpleportfoliomanager.menu.l1.MenuL1;
import com.wiktorkielar.simpleportfoliomanager.services.Email;
import com.wiktorkielar.simpleportfoliomanager.services.Encryption;
import com.wiktorkielar.simpleportfoliomanager.services.Validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuL2Register {

    public static final String MIN_10_CHARS = "*min 10 chars\n";
    public static final String MAX_100_CHARS = "*max 100 chars\n";
    public static final String MIN_2_CHARS = "*min 2 chars\n";
    public static final String MIN_5_CHARS = "*min 5 chars\n";
    public static final String CORRECT_EMAIL = "*correct e-mail format: (name@domain.com)\n";
    public static final String MIN_1_LC_CHAR = "*min 1 lower-case char\n";
    public static final String MIN_1_UC_CHAR = "*min 1 upper-case char\n";
    public static final String MIN_1_S_CHAR = "*min 1 special char\n";

    private static List<String> fieldList = new ArrayList<>();
    private static List<String> descriptionList = new ArrayList<>();
    private static List<String> valueList = new ArrayList<>();

    public static byte[] pass;
    public static byte[] salt;

    public static void start() {
        Menu menuL2Register = new Menu("Register New Account");
        menuL2Register.displayMenu();
        register();
    }

    private static void register() {
        listInitialization();
        registerUserInformation();

        if(Query.queryUsersForEmail(valueList.get(2)) == 1){
            System.out.println("Email already in the database!");
            start();
        } else {
        encryptUserPassword();
        Insert.insertIntoUsers(valueList, pass, salt);
        sendConfirmationEmail();
        MenuL1.start();
        }
    }

    //TESTED, OK
    private static void listInitialization() {
        initializeExternalList();
        initializeValueList();
        initializeFieldDescriptionList();
    }

    //TESTED, OK
    public static void registerUserInformation() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < fieldList.size(); i++) {
            while (valueList.get(i) == null) {

                System.out.println("\n" + fieldList.get(i) + ": ");
                System.out.println(descriptionList.get(i) + "Enter the string below: ");

                String inputString = scanner.nextLine();

                if (Validation.validateRegistrationString(fieldList.get(i), inputString)) {
                    if (i == 3 || i == 5) {
                        if (valueList.get(i - 1).equals(inputString)) {
                            valueList.set(i, inputString);
                        }
                    } else {
                        valueList.set(i, inputString);
                    }
                }
            }
        }
    }

    private static void sendConfirmationEmail() {
        if (valueList.get(2) != null && valueList.get(4) != null) {
            String toEmail = valueList.get(2);
            String subject = "Simple Portfolio Manager Account Activation";
            String content = "<h2>Hello " + valueList.get(0) + ",</h2><br>" +
                    "Please use the code below to active your Simple Portfolio Manager account:" +
                    "<br><br><b>" + Encryption.generateActivationCode(valueList.get(2)) + "</b>" +
                    "<br><br>Best regards," +
                    "<br>SPM Team";
            String userInfoSending = "\nSending activation email...";
            String userInfoSent = "\nActivation e-mail sent. To start using SPM please check your inbox" +
                    " and activate your account ([Activate] option in main menu).";
            Email.send(toEmail, subject, content, userInfoSending, userInfoSent);
        }
    }

    public static void encryptUserPassword() {
        salt = Encryption.generateSalt();
        pass = Encryption.encrypt(valueList.get(5), salt);

    }



    //TESTED, OK
    private static void initializeExternalList() {
        fieldList.add("FIRST NAME");
        fieldList.add("LAST NAME");
        fieldList.add("EMAIL");
        fieldList.add("REPEAT EMAIL");
        fieldList.add("PASSWORD");
        fieldList.add("REPEAT PASSWORD");
    }

    //TESTED, OK
    private static void initializeFieldDescriptionList() {
        descriptionList.add(MIN_2_CHARS +
                MAX_100_CHARS);
        descriptionList.add(MIN_2_CHARS +
                MAX_100_CHARS);
        descriptionList.add(MIN_5_CHARS +
                MAX_100_CHARS +
                CORRECT_EMAIL);
        descriptionList.add(MIN_5_CHARS +
                MAX_100_CHARS +
                CORRECT_EMAIL +
                "*matches provided e-mail address\n");
        descriptionList.add(MIN_10_CHARS +
                MAX_100_CHARS +
                MIN_1_LC_CHAR +
                MIN_1_UC_CHAR +
                MIN_1_S_CHAR);
        descriptionList.add(MIN_10_CHARS +
                MAX_100_CHARS +
                MIN_1_LC_CHAR +
                MIN_1_UC_CHAR +
                MIN_1_S_CHAR +
                "*matches already provided password\n");
    }

    //TESTED, OK
    private static void initializeValueList() {
        for (int i = 0; i < fieldList.size(); i++) {
            valueList.add(null);
        }
    }



}
