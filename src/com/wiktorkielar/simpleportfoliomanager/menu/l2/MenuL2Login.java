package com.wiktorkielar.simpleportfoliomanager.menu.l2;

import com.wiktorkielar.simpleportfoliomanager.database.Query;
import com.wiktorkielar.simpleportfoliomanager.menu.Menu;
import com.wiktorkielar.simpleportfoliomanager.menu.l3.MenuL3;
import com.wiktorkielar.simpleportfoliomanager.menu.l1.MenuL1;
import com.wiktorkielar.simpleportfoliomanager.services.Encryption;
import com.wiktorkielar.simpleportfoliomanager.services.Validation;

import java.util.Scanner;

public class MenuL2Login {

    private static Scanner scanner = new Scanner(System.in);
    private static String email;
    private static byte[] salt;
    private static byte[] pass;

    private MenuL2Login() {
    }

    public static void start() {
        Menu menuL2Login = new Menu("Login");
        menuL2Login.displayMenu();
        login();
    }

    public static void login() {
        verifyEmailAddress();
        queryDatabaseForSalt();
        encryptThePassword();
        queryDatabase();

    }

    private static void verifyEmailAddress() {
        email = null;
        while (email == null || !Validation.isEmail(email)) {
            System.out.println("EMAIL ADDRESS:");
            email = scanner.nextLine();
        }

        byte[] salt = Query.queryUsersForSalt(email);

        if (salt == null) {
            System.out.println("Please provide correct e-mail address.");
        }
    }

    private static void queryDatabaseForSalt(){
        if(Query.queryUsersForSalt(email) == null){
            System.out.println("There's no such email in the database!");
            email = null;
            MenuL1.start();
        } else {
            salt = Query.queryUsersForSalt(email);
        }
    }

    private static void encryptThePassword(){
        System.out.println("PASSWORD:");
        String password = scanner.nextLine();
        pass = Encryption.encrypt(password, salt);
    }

    private static void queryDatabase(){
        if(Query.queryUsersForPassMatch(email, pass, salt) == 1){
            System.out.println("\nYou have successfully logged in!\n");
            MenuL3.start();
        } else {
            System.out.println("\nPassword and email do not match.");
            MenuL1.start();
        }
        email = null;
        pass = null;
        salt = null;
    }
}

