package com.wiktorkielar.simpleportfoliomanager.services;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;

public final class Encryption {

    public static final int ITERATIONS = 10000;
    public static final int KEY_LENGTH = 256;
    public static final String ALGORITHM = "PBKDF2WithHmacSHA1";
    public static final String CLASS_NAME = Logging.getClassName();

    private Encryption() {
    }

    public static byte[] generateSalt() {
        SecureRandom secureRandom = new SecureRandom();
        return secureRandom.generateSeed(128);
    }

    public static byte[] encrypt(String password, byte[] salt) {

        byte[] encodedKey = null;
        KeySpec keySpec = new PBEKeySpec(password.toCharArray(), salt, ITERATIONS, KEY_LENGTH);

        try {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance(ALGORITHM);
            encodedKey = secretKeyFactory.generateSecret(keySpec).getEncoded();

        } catch (NoSuchAlgorithmException e) {
            Logging.log(CLASS_NAME, e, "Error generating secretKeyFactory instance.");
        } catch (InvalidKeySpecException e) {
            Logging.log(CLASS_NAME, e, "Error generating encodedKey instance.");
        }

        return encodedKey;
    }

    public static String generateActivationCode(String input) {

        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            byte[] hashArray = messageDigest.digest(input.getBytes());
            return Base64.getEncoder().encodeToString(hashArray).substring(0, 10);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String generatePasswordResetToken(){
        SecureRandom secureRandom = new SecureRandom();
        return Long.toUnsignedString(secureRandom.nextLong());
    }

}
