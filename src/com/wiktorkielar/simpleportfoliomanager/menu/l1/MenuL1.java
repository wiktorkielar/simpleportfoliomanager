package com.wiktorkielar.simpleportfoliomanager.menu.l1;

import com.wiktorkielar.simpleportfoliomanager.menu.*;
import com.wiktorkielar.simpleportfoliomanager.menu.l2.MenuL2Activate;
import com.wiktorkielar.simpleportfoliomanager.menu.l2.MenuL2Login;
import com.wiktorkielar.simpleportfoliomanager.menu.l2.MenuL2Register;
import com.wiktorkielar.simpleportfoliomanager.menu.l2.MenuL2Reset;

public class MenuL1 {

    private MenuL1(){
    }

    //TESTED, OK
    public static void start(){
        System.console().writer().println("\nSimple Portfolio Manager 1.0\n");
        Menu menuL1 = new Menu("Main");
        menuL1.addPosition("Login");
        menuL1.addPosition("Register");
        menuL1.addPosition("Activate");
        menuL1.addPosition("Reset Password");
        menuL1.addPosition("Quit");
        menuL1.displayMenu();
        jumpToMenu(menuL1.readUserInput());
    }

    //TESTED, OK
    private static void jumpToMenu(int option){
        if(option == 1){
            MenuL2Login.start();
        } else if (option == 2){
            MenuL2Register.start();
        } else if (option == 3){
            MenuL2Activate.start();
        } else if(option == 4){
            MenuL2Reset.start();
        } else {
            System.out.println("\nThank you for using SPM. Goodbye! ");
        }

    }
}
