package com.wiktorkielar.simpleportfoliomanager.database;

import com.wiktorkielar.simpleportfoliomanager.services.Logging;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.wiktorkielar.simpleportfoliomanager.database.Constants.*;

public class Delete {

    public static final String DELETE_FROM_TOKENS = STATEMENT_DELETE + " " + CLAUSE_FROM + " " + TABLE_TOKENS +
            " WHERE " + TABLE_TOKENS + "." + COLUMN_TOKEN_USER_ID + " IN (" +
            STATEMENT_SELECT + " " + TABLE_USERS + "." + COLUMN_USER_ID + " " + CLAUSE_FROM + " " + TABLE_USERS +
            " WHERE " + TABLE_USERS + "." + COLUMN_USER_EMAIL + " = ?)";

    private static PreparedStatement deleteFromTokensPreparedStatement;

    public static final String CLASS_NAME = Logging.getClassName();

    private Delete(){
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand, String email){
        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.setString(1, email);
            preparedStatement.execute();
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object.");
        }
    }

    public static void deleteFromTokens(String email){
        Constants.openConnection();
        execute(deleteFromTokensPreparedStatement, DELETE_FROM_TOKENS, email);
        Constants.closeConnection(deleteFromTokensPreparedStatement);
    }

}
