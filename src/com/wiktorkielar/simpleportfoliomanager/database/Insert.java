package com.wiktorkielar.simpleportfoliomanager.database;

import com.wiktorkielar.simpleportfoliomanager.objects.Exchange;
import com.wiktorkielar.simpleportfoliomanager.objects.Symbol;
import com.wiktorkielar.simpleportfoliomanager.services.Logging;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static com.wiktorkielar.simpleportfoliomanager.database.Constants.*;

public class Insert {


    public static final String VALUES = "VALUES";

    public static final String INSERT_INTO_USERS = STATEMENT_INSERT_INTO + " " +  TABLE_USERS + " ( " +
            COLUMN_USER_FIRST_NAME + ", " + COLUMN_USER_LAST_NAME + ", " +
            COLUMN_USER_EMAIL + ", " + COLUMN_USER_PASS + ", " +
            COLUMN_USER_SALT + ", " + COLUMN_USER_REGISTRATION_DATE + ", " +
            COLUMN_USER_ACTIVATED + ") " +
            VALUES + " ( ?, ?, ?, ?, ?, ?, ?)";

    public static final String INSERT_INTO_TRANSACTIONS = STATEMENT_INSERT_INTO + " " + TABLE_TRANSACTIONS + " ( " +
            COLUMN_TRANSACTION_USER_ID + ", " + COLUMN_TRANSACTION_SYMBOL_ID + ", " +
            COLUMN_TRANSACTION_BUY_DATE + ", " + COLUMN_TRANSACTION_BUY_PRICE + ", " +
            COLUMN_TRANSACTION_SELL_DATE + ", " + COLUMN_TRANSACTION_SELL_PRICE + ", " +
            COLUMN_TRANSACTION_VOLUME + ") " +
            VALUES + " ( ?, ?, ?, ?, ?, ?, ?)";


    public static final String INSERT_INTO_EXCHANGES = STATEMENT_INSERT_INTO + " " + TABLE_EXCHANGES + " ( " +
            COLUMN_EXCHANGE_SHORT_NAME + ", " + COLUMN_EXCHANGE_LONG_NAME + ") " +
            VALUES + " ( ?, ?)";

    public static final String INSERT_INTO_TOKENS = STATEMENT_INSERT_INTO + " " + TABLE_TOKENS + " ( " +
            COLUMN_TOKEN_USER_ID + ", " + COLUMN_TOKEN_TOKEN + ", " +
            COLUMN_TOKEN_CREATION_DATE + ") " +
            STATEMENT_SELECT + " " + TABLE_USERS + "." + COLUMN_USER_ID +", ?, ? " + CLAUSE_FROM +  " " +
            TABLE_USERS + " " + CLAUSE_WHERE + " " + COLUMN_USER_EMAIL + " =?";

    public static final String INSERT_INTO_SYMBOLS = STATEMENT_INSERT_INTO + " " + TABLE_SYMBOLS + " ( " +
            COLUMN_SYMBOL_NAME + ", " + COLUMN_SYMBOL_TICKER  + ", " +
            COLUMN_SYMBOL_MARKET_ID + ") " +
            VALUES + " ( ?, ?, ?)";


    private static PreparedStatement insertIntoUsersPreparedStatement;
    private static PreparedStatement insertIntoTransactionsPreparedStatement;
    private static PreparedStatement insertIntoExchangesPreparedStatement;
    private static PreparedStatement insertIntoTokensPreparedStatement;
    private static PreparedStatement insertIntoSymbolsPreparedStatement;


    public static final String CLASS_NAME = Logging.getClassName();

    private Insert(){
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand, List<String> insertIntoUsersInput, byte[] pass, byte[] salt){
        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.setString(1, insertIntoUsersInput.get(0));
            preparedStatement.setString(2, insertIntoUsersInput.get(1));
            preparedStatement.setString(3, insertIntoUsersInput.get(2));
            preparedStatement.setBytes(4, pass);
            preparedStatement.setBytes(5, salt);
            preparedStatement.setString(6, getTime(0));
            preparedStatement.setInt(7, 0);
            preparedStatement.execute();
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object");
        }
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand, String token, String email){
        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.setString(1, token);
            preparedStatement.setString(2, getTime(15));
            preparedStatement.setString(3, email);
            preparedStatement.execute();
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object");
        }
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand, Exchange exchange){
        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.setString(1, exchange.getShortName());
            preparedStatement.setString(2, exchange.getLongName());
            preparedStatement.execute();
        } catch (SQLException e) {
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object");
        }
    }

    private static void execute(PreparedStatement preparedStatement, String sqlCommand, Symbol symbol){
        try {
            preparedStatement = Constants.getConnection().prepareStatement(sqlCommand);
            preparedStatement.setString(1, symbol.getName());
            preparedStatement.setString(2, symbol.getTicker());
            preparedStatement.setInt(3, symbol.getMarket_id());
            preparedStatement.execute();
        } catch (SQLException e){
            Logging.log(CLASS_NAME, e, "Error creating parametrized PreparedStatement object");
        }
    }

    public static void insertIntoUsers(List<String> insertIntoUsersInput, byte[] pass, byte[] salt){
        Constants.openConnection();
        execute(insertIntoUsersPreparedStatement, INSERT_INTO_USERS, insertIntoUsersInput, pass, salt);
        Constants.closeConnection(insertIntoUsersPreparedStatement);
    }

    public static void insertIntoTokens(String email, String token){
        Constants.openConnection();
        execute(insertIntoTokensPreparedStatement, INSERT_INTO_TOKENS, token, email);
        Constants.closeConnection(insertIntoTokensPreparedStatement);
    }

    public static void insertIntoExchanges(Exchange exchange){
        Constants.openConnection();
        execute(insertIntoExchangesPreparedStatement, INSERT_INTO_EXCHANGES, exchange);
        Constants.closeConnection(insertIntoExchangesPreparedStatement);
    }

    public static void insertIntoSymbols(Symbol symbol){
        Constants.openConnection();
        execute(insertIntoSymbolsPreparedStatement, INSERT_INTO_SYMBOLS, symbol);
        Constants.closeConnection(insertIntoSymbolsPreparedStatement);
    }

}
