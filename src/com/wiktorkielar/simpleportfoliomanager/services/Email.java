package com.wiktorkielar.simpleportfoliomanager.services;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class Email {

    public static final String LOGIN = "simpleportfoliomanager";
    public static final String DOMAIN = "gmail.com";
    public static final String FROM_EMAIL_ADDRESS = LOGIN + "@" + DOMAIN;
    public static final String FROM_EMAIL_NAME = "Simple Portfolio Manager";
    public static final String HOST = "smtp." + DOMAIN;

    private static Properties properties;
    private static Session session;
    private static Message message;
    private static Address fromEmailAddress;
    private static Address toEmailAddress;

    private Email() {
    }

    private static void setProperties() {
        properties = new Properties();
        properties.setProperty("mail.smtp.auth", "true");
        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.smtp.host", HOST);
        properties.setProperty("mail.smtp.port", "587");
    }

    private static void setSession() {
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("simpleportfoliomanager", "thisisatestpassword");
            }
        };
        session = Session.getDefaultInstance(properties, authenticator);
    }

    private static void setMessage(String toEmail, String subject, String content) {
        message = new MimeMessage(session);

        try {
            fromEmailAddress = new InternetAddress(FROM_EMAIL_ADDRESS, FROM_EMAIL_NAME);
            toEmailAddress = new InternetAddress(toEmail);
        } catch (AddressException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        try {
            message.setFrom(fromEmailAddress);
            message.setRecipient(Message.RecipientType.TO, toEmailAddress);
            message.setSubject(subject);
            message.setContent(content, "text/html");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private static void userInfoSending(String userInfoSending) {
        if (userInfoSending != null) {
            System.out.println(userInfoSending);
        }
    }

    private static void transport() {
        try {
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private static void userInfoSent(String userInfoSent) {
        if (userInfoSent != null) {
            Validation.splitIntoRowsAndDisplay(userInfoSent, 45);
        }
    }

    public static void send(String toEmail, String subject, String content, String userInfoSending, String
            userInfoSent) {
        setProperties();
        setSession();
        setMessage(toEmail, subject, content);
        userInfoSending(userInfoSending);
        transport();
        userInfoSent(userInfoSent);
    }


}
