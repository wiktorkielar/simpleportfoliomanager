package com.wiktorkielar.simpleportfoliomanager.menu.l3;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.TextPage;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;
import com.wiktorkielar.simpleportfoliomanager.database.Insert;
import com.wiktorkielar.simpleportfoliomanager.database.Query;
import com.wiktorkielar.simpleportfoliomanager.menu.Menu;
import com.wiktorkielar.simpleportfoliomanager.objects.Exchange;
import com.wiktorkielar.simpleportfoliomanager.objects.Symbol;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MenuL3 {

    public static final String LOCAL_TXT_PATH = "src/com/wiktorkielar/simpleportfoliomanager/resources/txt/";

    public static final String EOD_EXCHANGES_URL = "http://www.eoddata.com/default.aspx";
    public static final String EOD_LOGIN_URL = "http://www.eoddata.com/default.aspx";
    public static final String EOD_DATA_URL = "http://www.eoddata.com/Data/symbollist.aspx?e=";

    public static final String EOD_LOGIN_ELEMENT = "ctl00$cph1$lg1$txtEmail";
    public static final String EOD_LOGIN_STRING = "simpleportfolio";

    public static final String EOD_PASSWORD_ELEMENT = "ctl00$cph1$lg1$txtPassword";
    public static final String EOD_PASSWORD_STRING = "thisisatestpassword";

    public static final String EOD_BUTTON_ELEMENT = "ctl00$cph1$lg1$btnLogin";

    private static String html;
    private static List<Exchange> exchangesList = new ArrayList<>();
    private static List<Symbol> symbolsList = new ArrayList<>();

    private MenuL3(){
    }

    public static void start(){

        if(Query.queryExchangesForCount() == 0){
            loadExchangesIntoDatabase();
        }

        if(Query.querySymbolsForCount() == 0){
            loadSymbolsIntoDatabase();
        }

        Menu menuL3 = new Menu("Portfolio");
        menuL3.addPosition("Browse Portfolio");
        menuL3.addPosition("Add Instrument");
        menuL3.addPosition("Remove Instrument");
        menuL3.addPosition("Account Settings");
        menuL3.displayMenu();
    }

    public static void loadExchangesIntoDatabase() {
        System.out.println("This may take a while:\n");
        System.out.print("Loading exchange list... ");
        downloadRawExchangesListAsHTML();
        parseExchangesList();
        insertExchanges();
        System.out.println("done.");
    }

    public static void downloadRawExchangesListAsHTML() {


        StringBuilder lines = new StringBuilder();
        try {
            URL url = new URL(EOD_EXCHANGES_URL);
            try (InputStream inputStream = url.openStream()) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String line;

                while ((line = bufferedReader.readLine()) != null) {
                    lines.append(line);
                    lines.append("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        html = lines.toString();
    }

    public static void parseExchangesList(){
        List<Exchange> exchanges = new ArrayList<>();

        String shortNameRegex = "\\/stocklist\\/[A-Z]+";
        Pattern shortNamePattern = Pattern.compile("(?=(" + shortNameRegex + "))");
        Matcher shortNameMatcher;

        String fullNameRegex = "<\\/td><td>.+<\\/td><td>";
        Pattern fullNamePattern = Pattern.compile("(?=(" + fullNameRegex + "))");
        Matcher fullNameMatcher;

        String[] inputArray = html.split("\\n");

        for (String string : inputArray){
            if(string.contains("/stocklist/")){

                Exchange exchange = new Exchange();

                shortNameMatcher = shortNamePattern.matcher(string);
                while (shortNameMatcher.find()){
                    String patternMatchedString   = shortNameMatcher.group(1);
                    exchange.setShortName(patternMatchedString.replace("/stocklist/", ""));
                }

                fullNameMatcher = fullNamePattern.matcher(string);
                while (fullNameMatcher.find()){
                    String patternMatchedString = fullNameMatcher.group(1);
                    exchange.setLongName(patternMatchedString.replace("</td><td>", ""));
                }

                exchanges.add(exchange);
            }
        }

        exchangesList = exchanges;
    }

    public static void insertExchanges() {
        for (Exchange exchange : exchangesList) {
            Insert.insertIntoExchanges(exchange);
        }

    }

    public static void loadSymbolsIntoDatabase(){
        System.out.print("Loading symbol list for each exchange... ");
        downloadRawSymbolsListAsTXT();
        parseAndInsertSymbolsList();
        System.out.println("done.");
    }

    public static void downloadRawSymbolsListAsTXT() {
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(java.util.logging.Level.OFF);

        try (WebClient webClient = new WebClient(BrowserVersion.CHROME);) {

            HtmlPage loginHtmlPage = webClient.getPage(EOD_LOGIN_URL);

            HtmlTextInput loginHtmlTextInput = loginHtmlPage.getElementByName(EOD_LOGIN_ELEMENT);
            loginHtmlTextInput.setValueAttribute(EOD_LOGIN_STRING);

            HtmlPasswordInput passwordHtmlTextInput = loginHtmlPage.getElementByName(EOD_PASSWORD_ELEMENT);
            passwordHtmlTextInput.setValueAttribute(EOD_PASSWORD_STRING);

            HtmlSubmitInput htmlSubmitInput = loginHtmlPage.getElementByName(EOD_BUTTON_ELEMENT);
            HtmlPage dataHtmlPage = htmlSubmitInput.click();

            for(Exchange exchange : exchangesList){
                String shortName = exchange.getShortName();
                TextPage textData = webClient.getPage(EOD_DATA_URL + shortName);
                String outputString = textData.getContent().toString();
                try (PrintWriter printWriter = new PrintWriter(LOCAL_TXT_PATH + shortName + ".txt");) {
                    printWriter.println(outputString);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public static void parseAndInsertSymbolsList(){
        for(Exchange exchange : exchangesList){
            exchange.setId(Query.queryExchangesForId(exchange.getShortName()));
            String shortName = exchange.getShortName();
            try (FileReader fileReader = new FileReader(LOCAL_TXT_PATH + shortName + ".txt");) {
                BufferedReader bufferedReader = new BufferedReader(fileReader);
                String line;

                while ((line = bufferedReader.readLine()) != null){
                    if(!line.contains("Symbol\tDescription") || !line.contains("")){
                        String[] lineAsArray = line.split("\t");
                        Symbol symbol = new Symbol();
                        symbol.setTicker(lineAsArray[0]);
                        if(lineAsArray.length > 1){
                            symbol.setName(lineAsArray[1]);
                        } else {
                            symbol.setName("Undefined");
                        }
                        symbol.setMarket_id(exchange.getId());
                        Insert.insertIntoSymbols(symbol);
                        symbol = null;
                        System.gc();

                    }
                }
                bufferedReader = null;

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


}



