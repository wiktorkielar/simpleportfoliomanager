package com.wiktorkielar.simpleportfoliomanager.menu.l2;

import com.wiktorkielar.simpleportfoliomanager.database.Delete;
import com.wiktorkielar.simpleportfoliomanager.database.Insert;
import com.wiktorkielar.simpleportfoliomanager.database.Query;
import com.wiktorkielar.simpleportfoliomanager.database.Update;
import com.wiktorkielar.simpleportfoliomanager.menu.Menu;
import com.wiktorkielar.simpleportfoliomanager.menu.l1.MenuL1;
import com.wiktorkielar.simpleportfoliomanager.services.Email;
import com.wiktorkielar.simpleportfoliomanager.services.Encryption;
import com.wiktorkielar.simpleportfoliomanager.services.Validation;

import java.util.Scanner;

public class MenuL2Reset {

    private static final Scanner scanner = new Scanner(System.in);
    private static String email;
    private static String token;
    private static String attemptedPass;
    private static String attemptedPassRepeated;
    private static byte[] salt;
    private static byte[] pass;

    public static void start() {
        Menu menuL2Reset = new Menu("Reset Password");
        menuL2Reset.displayMenu();
        reset();
    }

    private static void reset() {
        verifyEmailAddress();
        queryUsersForEmail();
        generateToken();
        Delete.deleteFromTokens(email);
        Insert.insertIntoTokens(email, token);
        sendConfirmationEmail();
        verifyToken();
        validateNewPasswords();
        prepareNewPasswords();
        Update.updateUsersSaltAndPassword(salt, pass, email);

    }

    private static void verifyEmailAddress() {
        email = null;
        while (email == null || !Validation.isEmail(email)) {
            System.out.println("EMAIL ADDRESS:");
            email = scanner.nextLine();
        }
    }

    private static void queryUsersForEmail() {
        if (email != null) {
            if (Query.queryUsersForEmail(email) == 0) {
                System.out.println("\nThere's no such email address in the database!");
                verifyEmailAddress();
            } else {
                System.out.println("\nEmail address correct!");
            }
        }
    }

    private static void generateToken() {
        token = Encryption.generatePasswordResetToken();
        System.out.println("\nNew token has been generated!\n");
    }

    private static String queryUsersForName(String email) {
        return Query.queryUsersForName(email);
    }

    private static void sendConfirmationEmail() {
        String subject = "Simple Portfolio Manager Account Password Reset";
        String content = "<h2>Hello " + queryUsersForName(email) + ",</h2><br>" +
                "Please use the code below to reset password to your Simple Portfolio Manager account:" +
                "<br><br><b>" + token + "</b>" +
                "<br><br>Best regards," +
                "<br>SPM Team";
        String userInfoSending = "\nSending password reset email...";
        String userInfoSent = "\nPassword reset e-mail sent. To rest your SPM account password please check your " +
                "inbox" +
                " and copy password reset token below. Please note that your token will expire in next 15 minutes. " +
                "If that's the case, please just generate a new one.\n";
        Email.send(email, subject, content, userInfoSending, userInfoSent);
    }

    private static void verifyToken() {
        System.out.println("\nTOKEN:\n");
        String attemptedToken = scanner.nextLine();
        if (attemptedToken.equals(token)) {
            System.out.println("\nTokens match!\n");
        } else {
            System.out.println("\nTokens do not match!\n");
            MenuL1.start();
        }
    }

    private static void validateNewPasswords() {
        System.out.println("\nNEW PASSWORD\n");

        while (attemptedPass == null || !Validation.validateRegistrationString("PASSWORD", attemptedPass)) {
            attemptedPass = scanner.nextLine();
        }
        System.out.println("\nREPEAT NEW PASSWORD\n");
        while (attemptedPassRepeated == null || !Validation.validateRegistrationString("PASSWORD",
                attemptedPassRepeated)){
            attemptedPassRepeated = scanner.nextLine();
        }

        if (!attemptedPass.equals(attemptedPassRepeated)) {
            System.out.println("\nPasswords do not match!");
            MenuL1.start();
        }
    }

    private static void prepareNewPasswords(){
        if(attemptedPass != null && attemptedPassRepeated != null){
            salt = Encryption.generateSalt();
            pass = Encryption.encrypt(attemptedPass, salt);
        }

    }

}
