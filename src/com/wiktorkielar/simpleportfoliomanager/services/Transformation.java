package com.wiktorkielar.simpleportfoliomanager.services;


import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;

public class Transformation {

    public static final String STYLESHEET_NAME = "XMLStylesheet";

    public static final String PROJECT_PATH = "src/com/wiktorkielar/simpleportfoliomanager";
    public static final String STYLESHEET_PATH = PROJECT_PATH + "/resources/";
    public static final String XML_PATH = PROJECT_PATH + "/logs/xml/";
    public static final String HTML_PATH = PROJECT_PATH + "/logs/html/";

    private Transformation() {
    }

    public static void start(String className) {
        File xslFile = new File(STYLESHEET_PATH + STYLESHEET_NAME + ".xsl");
        StreamSource xslStreamSource = new StreamSource(xslFile);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        try {
            Transformer transformer = transformerFactory.newTransformer(xslStreamSource);
            File xmlFile = new File(XML_PATH + className + ".xml");

            StreamSource xmlStreamSource = new StreamSource(xmlFile);
            File htmlFile = new File(HTML_PATH + className + ".html");

            StreamResult streamResult = new StreamResult(htmlFile);
            transformer.transform(xmlStreamSource, streamResult);
        } catch (TransformerException e) {
            System.out.println(e.getMessage());
        }


    }

}
